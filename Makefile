# baro - 2014/2015 #

# module purge
# module load gnu hwloc numactl openmpi
# module list
#	Currently Loaded Modulefiles:
#	  1) gnu/4.9.2                 2) hwloc/1.10.0              3) numactl/2.0.10            4) openmpi/1.8.3/gnu/4.9.2

_: all

all clean run:
	for dir in */ ; do cd $$dir && { make $@ || exit 1 ; cd - ; } ; done

distall: clean
	for dir in */ ; do cd $$dir && { make all ; cd - ; } ; done ; :

#EOF
