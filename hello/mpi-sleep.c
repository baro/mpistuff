/*
** baro - 20141212
*/

// module load openmpi
// mpicc -Wall ~/mpi-sleep.c -o ~/bin/mpi-sleep
// mpirun -np 16 ~/bin/mpi-sleep 60 &
// mpirun -np 1 ~/bin/mpi-sleep 60

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <mpi.h>

int main ( int argc , char **argv )
{
	int rank=0, nprocs=1;
	char hostname[MPI_MAX_PROCESSOR_NAME];
	int hostname_len = 0;

	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD,&nprocs);
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);

	MPI_Get_processor_name( hostname , &hostname_len );
	if (hostname_len==0)
		gethostname( hostname , MPI_MAX_PROCESSOR_NAME );

	printf("%s: mpi process %d of %d\n", hostname, rank+1, nprocs);

	if ( argc > 0 )
	{
		if ( rank == 0 )
			sleep(atoi(argv[1]));
		MPI_Barrier(MPI_COMM_WORLD);
	}

	MPI_Finalize();

	return 0;
}

//EOF
