/*
** baro - 20150327
*/

#include <stdio.h>
#include <hwloc.h>

int main(void)
{
	int hwloc_system, hwloc_machine, hwloc_numanodes, hwloc_sockets, hwloc_cores, hwloc_pus;
	hwloc_topology_t topology;

	hwloc_topology_init(&topology);
	hwloc_topology_load(topology);

	hwloc_system	= hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_SYSTEM);
	hwloc_machine	= hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_MACHINE);
	hwloc_numanodes	= hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_NODE);
	hwloc_sockets	= hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_SOCKET);
	hwloc_cores	= hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE);
	hwloc_pus	= hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_PU);

	printf( "hwloc.system=%d hwloc.machine=%d hwloc.numanodes=%d hwloc.sockets=%d hwloc.cores=%d hwloc.pus=%d\n"
		, hwloc_system , hwloc_machine , hwloc_numanodes , hwloc_sockets , hwloc_cores , hwloc_pus );

	hwloc_topology_destroy(topology);

	return 0;
}

//EOF
