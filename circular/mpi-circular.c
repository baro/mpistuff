/*
** baro - 20150328
*/

// module load openmpi
// mpicc -Wall ./mpi-circular.c -o ./mpi-circular
// mpirun ./mpi-circular

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <mpi.h>

unsigned generate( unsigned in )
{
	return in+1;
}

int main ( int argc , char **argv )
{
	int rank=0, size=1;
	char hostname[MPI_MAX_PROCESSOR_NAME];
	int hostname_len = 0;

	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD,&size);
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);

	MPI_Get_processor_name( hostname , &hostname_len );
	if (hostname_len==0)
		gethostname( hostname , MPI_MAX_PROCESSOR_NAME );

	{
		int proc_next = ( rank + 1 + size ) % size;
		int proc_prev = ( rank - 1 + size ) % size;
		int proc_curr = rank;

		unsigned int seed = 0U;

		printf(	"0 host=%s rank=%d size=%d next=%d prev=%d seed=%d\n"
			, hostname, rank, size, proc_next, proc_prev, seed);

		if ( rank == 0 )
		{
			seed = generate( seed );
			MPI_Send( &seed , 1 , MPI_UNSIGNED , proc_next , proc_next , MPI_COMM_WORLD );
			printf(	"1 host=%s rank=%d size=%d next=%d prev=%d seed=%d\n"
				, hostname, rank, size, proc_next, proc_prev, seed);
			MPI_Recv( &seed , 1 , MPI_UNSIGNED , proc_prev , proc_curr , MPI_COMM_WORLD , MPI_STATUS_IGNORE );
		}
		else
		{
			MPI_Recv( &seed , 1 , MPI_UNSIGNED , proc_prev , proc_curr , MPI_COMM_WORLD , MPI_STATUS_IGNORE );
			seed = generate( seed );
			printf(	"1 host=%s rank=%d size=%d next=%d prev=%d seed=%d\n"
				, hostname, rank, size, proc_next, proc_prev, seed);
			MPI_Send( &seed , 1 , MPI_UNSIGNED , proc_next , proc_next , MPI_COMM_WORLD );
		}

		MPI_Barrier(MPI_COMM_WORLD);

		printf(	"2 host=%s rank=%d size=%d next=%d prev=%d seed=%d\n"
			, hostname, rank, size, proc_next, proc_prev, seed);

		MPI_Bcast(&seed, 1, MPI_UNSIGNED, 0 , MPI_COMM_WORLD);

		MPI_Barrier(MPI_COMM_WORLD);

		printf(	"3 host=%s rank=%d size=%d next=%d prev=%d seed=%d\n"
			, hostname, rank, size, proc_next, proc_prev, seed);
	}

	MPI_Finalize();
	return 0;
}


/*
		if ( rank != 0 )
		{
			MPI_Recv(&seed, 1, MPI_INT, rank - 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			printf("Process %d received token %d from process %d\n", rank, seed, rank - 1);
			seed = generate( seed );
		}
		else
		{
			// Set the token's value if you are process 0
			seed = generate( seed );
		}
		MPI_Send(&seed, 1, MPI_INT, (rank + 1) % size, 0, MPI_COMM_WORLD);

		// Now process 0 can receive from the last process.
		if ( rank == 0 )
		{
			MPI_Recv( &seed, 1, MPI_INT, size - 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
			printf("Process %d received token %d from process %d\n", rank, seed, size - 1);
		}
*/

//EOF
