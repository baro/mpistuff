/*
** baro - 20150316
*/

#include <stdio.h>
#include <unistd.h>
#include <mpi.h>

int main(int argc, char * argv[])
{
	double finish, start, early, late, maxt, mint, avgt, th_time;
	int size, rank;
	char hostname[MPI_MAX_PROCESSOR_NAME];
	int hostname_len = 0;

	MPI_Init( &argc, &argv );
	MPI_Comm_size( MPI_COMM_WORLD, &size );
	MPI_Comm_rank( MPI_COMM_WORLD, &rank );
	MPI_Get_processor_name( hostname , &hostname_len );
	if (hostname_len==0)
		gethostname( hostname , MPI_MAX_PROCESSOR_NAME );

	start = MPI_Wtime();
	sleep(1);
	finish = MPI_Wtime();
	th_time = finish-start;

	MPI_Barrier(MPI_COMM_WORLD);

// next 2 make sense if same node (100% sure time sync'ed)
	MPI_Reduce( &start   , &early , 1 , MPI_DOUBLE_PRECISION , MPI_MIN , 0 , MPI_COMM_WORLD );
	MPI_Reduce( &finish  , &late  , 1 , MPI_DOUBLE_PRECISION , MPI_MAX , 0 , MPI_COMM_WORLD );
// next 3 make sense if multiple nodes (when not sure time sync'ed)
	MPI_Reduce( &th_time , &maxt , 1 , MPI_DOUBLE_PRECISION , MPI_MAX , 0 , MPI_COMM_WORLD );
	MPI_Reduce( &th_time , &mint , 1 , MPI_DOUBLE_PRECISION , MPI_MIN , 0 , MPI_COMM_WORLD );
	MPI_Reduce( &th_time , &avgt , 1 , MPI_DOUBLE_PRECISION , MPI_SUM , 0 , MPI_COMM_WORLD );

	printf( "rank %d running on %s, walltime: %f\n" , rank , hostname , th_time );

	fflush(stdout);
	usleep(10000);
	MPI_Barrier(MPI_COMM_WORLD);

	if ( rank == 0 )
	{
		printf( "Computation timings (s)\n" );
		printf( "MinPart=%f\n" , mint );
		printf( "MaxPart=%f\n" , maxt );
		printf( "Avg=%f\n" , avgt/size );
		printf( "MaxTot=%f\n" , late-early );
	}

	MPI_Finalize();

	return 0;
}

//EOF
