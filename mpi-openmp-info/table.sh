# baro - 20141206 #

labels="mpi.host mpi.r mpi.s omp.t omp.n omp.l omp.m pid ppid n.n n.mc n.mac n.man n.c s.c nc aff.mask a.c h.s h.m h.n h.s h.c h.l"
fmt="%-8.8s %5s %5s %5s %5s %5s %5s %5s %5s %3s %4s %5s %5s %3s %3s %3s %10.10s %3s %3s %3s %3s %3s %3s %3s\n"

echo >&2 '# see ./labels for details'
printf "${fmt}" $labels
sort -t= -n -k 2,2 -k 3,3 -k 5,5 | sed -r 's/[^ ]+=//g' | xargs -n 24 printf "${fmt}"
#printf "${fmt}" $labels

#EOF
