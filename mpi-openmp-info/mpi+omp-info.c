/*
** baro - 20141206
*/

// module load openmpi
// mpicc -Wall -fopenmp ~/mpi+omp-info.c -o ~/bin/mpi+omp-info
/*
	NUMA  = -DHAVE_NUMA  -I ${NUMACTL_HOME}/include
	HWLOC = -DHAVE_HWLOC -I ${HWLOC_HOME}/include -L ${HWLOC_HOME}/lib -lhwloc
	~/bin/mpi+omp-info: ~/mpi/mpi+omp-info.c
		mpicc -Wall -fopenmp $^ -o $@ $(HWLOC) $(NUMA)
	test:
		@fmt="%8.8s %5s %5s %5s %5s %5s %5s %5s %5s %3s %4s %5s %5s %3s %3s %3s %10.10s %3s %3s %3s %3s %3s %3s %3s\n" ; \
		printf "$$fmt" mpi.host mpi.r mpi.s omp.t omp.n omp.l omp.m pid ppid n.n n.mc n.mac n.man n.c s.c nc aff.mask a.c h.s h.m h.n h.s h.c h.l ; \
		OMP_NUM_THREADS=10 mpirun --map-by socket --bind-to socket -np 2 ~/bin/mpi+omp-info | sort -t= -n -k 2,2 -k 3,3 -k 5,5 | sed -r 's/[^ ]+=//g' | xargs -n 24 printf "$$fmt"
*/
// OMP_NUM_THREADS=10 mpirun --display-map --report-bindings --map-by socket --bind-to socket -np 2 ~/bin/mpi+omp-info | sort -t= -n -k 2,2 -k 3,3 -k 5,5
// OMP_NUM_THREADS=10 mpirun --map-by socket --bind-to socket -np 2 ~/bin/mpi+omp-info | sort -t= -n -k 2,2 -k 3,3 -k 5,5
// ps wwwax -mo pid,tid,thcount,psr,pcpu,cmd

//#define HAVE_NUMA
//#define HAVE_HWLOC

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <mpi.h>
#include <omp.h>
#include <sched.h>
#ifdef HAVE_NUMA
# include <numa.h>
#endif
#ifdef HAVE_HWLOC
# include <hwloc.h>
#endif

#ifdef _OPENMP
typedef struct
{
	int tid;
	int nth;
} omp_t;

typedef struct
{
	int lim;
	int max;
} omp_lim_t;

omp_t omp;
omp_lim_t omp_lim;
#endif

extern int sched_getaffinity(pid_t pid, size_t cpusetsize, cpu_set_t *mask);

int main ( int argc , char * argv[] )
{
	int rank=0, size=1;
	char hostname[MPI_MAX_PROCESSOR_NAME];
	int hostname_len = 0;
#ifdef HAVE_HWLOC
	int hwloc_system, hwloc_machine, hwloc_numanodes, hwloc_sockets, hwloc_cores, hwloc_pus;
	hwloc_topology_t topology;
#endif

	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD,&size);
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);

//	gethostname(hostname, MPI_MAX_PROCESSOR_NAME);

	memset( hostname , 0x0 , MPI_MAX_PROCESSOR_NAME );
	MPI_Get_processor_name( hostname , &hostname_len );
	memset( hostname + hostname_len , 0x0 , MPI_MAX_PROCESSOR_NAME - hostname_len );

//	int provided;
//	MPI_Init_thread(&argc,&argv,MPI_THREAD_SERIALIZED,&provided);

#ifdef HAVE_HWLOC
	hwloc_topology_init(&topology);
	hwloc_topology_load(topology);

	hwloc_system	= hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_SYSTEM);
	hwloc_machine	= hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_MACHINE);
	hwloc_numanodes	= hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_NODE);
	hwloc_sockets	= hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_SOCKET);
	hwloc_cores	= hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE);
	hwloc_pus	= hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_PU);

	hwloc_topology_destroy(topology);
#endif

#ifdef _OPENMP
# pragma omp parallel private(omp) shared(omp_lim)
#endif
	{
		int sched_cpu = sched_getcpu();
#ifdef HAVE_NUMA
		int numa_nodes                           = numa_num_configured_nodes();
		int numa_max_cpus                        = numa_num_configured_cpus();
		int numa_max_cpus_allowed_for_this_task  = numa_num_task_cpus();
		int numa_max_nodes_allowed_for_this_task = numa_num_task_nodes();
		int numa_current_node                    = numa_node_of_cpu(sched_cpu);
#endif

		/*int aff_ret = 0;*/
		int aff_max_cpus = 1024;
		int aff_len = CPU_ALLOC_SIZE(aff_max_cpus);
		cpu_set_t * aff_mask = CPU_ALLOC(aff_max_cpus);

		int ncpus = sysconf(_SC_NPROCESSORS_ONLN);

		CPU_ZERO(aff_mask);
		/*aff_ret = */sched_getaffinity((pid_t)0, aff_len, aff_mask);

#ifdef _OPENMP
		omp.tid = omp_get_thread_num();
		omp.nth = omp_get_num_threads();
		omp_lim.max = omp_get_max_threads();
		omp_lim.lim = omp_get_thread_limit();

# pragma omp master
		if ( rank == 0 && omp_lim.lim < omp_lim.max )
			fprintf( stderr , "*** %s: OMP_THREAD_LIMIT is %d, would have run %d threads\n"
			                , argv[0] , omp_lim.lim , omp_lim.max );

		if ( omp_lim.lim == 2147483647 )	// (1L<<31)-1
			omp_lim.lim = -1;		// just to avoid a useless huge number in output
#endif

		fprintf( stdout	, "mpi.host=%s mpi.rank=%d mpi.size=%d "
#ifdef _OPENMP
				  "omp.tid=%d omp.nth=%d omp_lim.lim=%d omp_lim.max=%d "
#else
				  "omp.disabled "
#endif
				  "pid=%u ppid=%u "
#ifdef HAVE_NUMA
				  "numa.nodes=%d "
				  "numa.max_cpus=%d "
				  "numa.max_cpus_allowed_for_this_task=%d "
				  "numa.max_nodes_allowed_for_this_task=%d "
				  "numa.current_node=%d "
#else
				  "numa.disabled "
#endif
				  "sched.cpu=%d ncpus=%d "
				  "aff.mask=0x%08lx aff.count=%d "
#ifdef HAVE_HWLOC
				  "hwloc.system=%d hwloc.machine=%d hwloc.numanodes=%d hwloc.sockets=%d hwloc.cores=%d hwloc.pus=%d "
#else
				  "hwloc.disabled "
#endif
				  "\n"
				, hostname , rank+1 , size
#ifdef _OPENMP
				, omp.tid+1 , omp.nth , omp_lim.lim , omp_lim.max
#endif
				, getpid() , getppid()
#ifdef HAVE_NUMA
				, numa_nodes
				, numa_max_cpus
				, numa_max_cpus_allowed_for_this_task
				, numa_max_nodes_allowed_for_this_task
				, numa_current_node
#endif
				, sched_cpu , ncpus
				, aff_mask->__bits[0] , CPU_COUNT_S(aff_len, aff_mask)
#ifdef HAVE_HWLOC
				, hwloc_system, hwloc_machine, hwloc_numanodes, hwloc_sockets, hwloc_cores, hwloc_pus
#endif
		);

		CPU_FREE(aff_mask);
	}

	MPI_Finalize();

	return 0;
}

//EOF
