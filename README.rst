# baro - 2014/2015 #

Sample MPI programs that I wrote for the MHPC and for
testing the cluster environments that I maintain.

Some bits of code may come from external sources (e.g.:
the used and reused hello world example, mpi-hello.c).

Feel free to use the provided code and adapt it at will.

If you find a bug, please drop me a line:
	baro AT democritos DOT it

#EOF
