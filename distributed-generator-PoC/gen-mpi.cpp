/*
** baro - 20150328
*/

// module load openmpi
// mpicxx -std=c++11 -Wall ./gen-mpi.cpp -o ./gen-mpi.x

#include <iostream>
#include <random>
#include <sstream>
#include <mpi.h>

typedef struct {
	int rank;
	int size;
	int hostname_len;
	char hostname[MPI::MAX_PROCESSOR_NAME];
	struct {
		int next;
		int prev;
		int curr;
	} proc;
} mpi_t;

mpi_t mpi = {
	.rank = 0 ,
	.size = 1 ,
	.hostname_len = 0 ,
	.hostname = { '\0' } ,
	.proc = { .next = 0 , .prev = 0 , .curr = 0 }
};

unsigned generate ( unsigned seed , int n )
{
	std::default_random_engine generator( seed );
	std::uniform_real_distribution<double> distribution( 0.0 , 100.0 );
	for ( int i = 0 ; i < n ; i++ )
		std::cout
			<< __FILE__ << ":" << __LINE__ << ":" << __FUNCTION__ << "(): "
			<< " mpi.rank=" << mpi.rank
			<< " mpi.size=" << mpi.size
			<< " random[" << i << "]=" << distribution( generator )
			<< std::endl;
	std::stringstream ss;
	ss << generator;
	ss >> seed;
	return seed;
}

int main( int argc , char * argv[] )
{
	unsigned seed = 1U;

	MPI::Init(argc, argv);
	mpi.rank = MPI::COMM_WORLD.Get_rank();
	mpi.size = MPI::COMM_WORLD.Get_size();

	memset( mpi.hostname , 0x0 , MPI::MAX_PROCESSOR_NAME );
	MPI::Get_processor_name( mpi.hostname , mpi.hostname_len );
	memset( mpi.hostname + mpi.hostname_len , 0x0 , MPI::MAX_PROCESSOR_NAME - mpi.hostname_len );

	mpi.proc.next = ( mpi.rank + 1 + mpi.size ) % mpi.size;
	mpi.proc.prev = ( mpi.rank - 1 + mpi.size ) % mpi.size;
	mpi.proc.curr = mpi.rank;

	if ( mpi.rank == 0 )
		generate( seed , mpi.size*2 );

	MPI::COMM_WORLD.Barrier();

//	generator.seed( seed );

	printf(	"0 mpi.host=%s mpi.rank=%d mpi.size=%d mpi.proc.prev=%d mpi.proc.curr=%d mpi.proc.next=%d seed=%d\n"
		, mpi.hostname , mpi.rank , mpi.size , mpi.proc.prev , mpi.proc.curr , mpi.proc.next , seed );

	if ( mpi.rank == 0 )
	{
		seed = generate( seed , 2 );
		MPI::COMM_WORLD.Send( &seed , 1 , MPI::UNSIGNED , mpi.proc.next , mpi.proc.next );
		printf(	"1 mpi.host=%s mpi.rank=%d mpi.size=%d mpi.proc.prev=%d mpi.proc.curr=%d mpi.proc.next=%d seed=%d\n"
			, mpi.hostname , mpi.rank , mpi.size , mpi.proc.prev , mpi.proc.curr , mpi.proc.next , seed );
		MPI::COMM_WORLD.Recv( &seed , 1 , MPI::UNSIGNED , mpi.proc.prev , mpi.proc.curr );
	}
	else
	{
		MPI::COMM_WORLD.Recv( &seed , 1 , MPI::UNSIGNED , mpi.proc.prev , mpi.proc.curr );
		seed = generate( seed , 2 );
		printf(	"1 mpi.host=%s mpi.rank=%d mpi.size=%d mpi.proc.prev=%d mpi.proc.curr=%d mpi.proc.next=%d seed=%d\n"
			, mpi.hostname , mpi.rank , mpi.size , mpi.proc.prev , mpi.proc.curr , mpi.proc.next , seed );
		MPI::COMM_WORLD.Send( &seed , 1 , MPI::UNSIGNED , mpi.proc.next , mpi.proc.next );
	}

	MPI::COMM_WORLD.Barrier();

	printf(	"2 mpi.host=%s mpi.rank=%d mpi.size=%d mpi.proc.prev=%d mpi.proc.curr=%d mpi.proc.next=%d seed=%d\n"
		, mpi.hostname , mpi.rank , mpi.size , mpi.proc.prev , mpi.proc.curr , mpi.proc.next , seed );

	MPI::COMM_WORLD.Bcast( &seed , 1 , MPI::UNSIGNED , 0 );

	MPI::COMM_WORLD.Barrier();

	printf(	"3 mpi.host=%s mpi.rank=%d mpi.size=%d mpi.proc.prev=%d mpi.proc.curr=%d mpi.proc.next=%d seed=%d\n"
		, mpi.hostname , mpi.rank , mpi.size , mpi.proc.prev , mpi.proc.curr , mpi.proc.next , seed );

	MPI::Finalize();

	return 0;
}

//EOF
