/*
** baro - 20150328
*/

#include <iostream>
#include <random>
#include <sstream>

// g++ ./gen.cpp -std=c++11 -o ./gen.x

int main( int argc , const char * const * argv )
{
	unsigned seed = 1U;
	int n = 10;

	std::default_random_engine generator (seed);

	std::cout << "initial generator seed = " << generator << std::endl;

	std::uniform_real_distribution<double> distribution (0.0,100.0);

	std::cout << "seed=" << generator << " rnddata["<<n<<"]( ";
	for (int i=0; i<n; ++i)
		std::cout << distribution(generator) << " ";
	std::cout << ")" << std::endl;

	std::cout << "generator seed after " << n << " numbers: " << generator << std::endl;

	unsigned seed_uint;
	std::stringstream ss;
	ss << generator;
	ss >> seed_uint;
	std::cout << "saved (current) seed: " << seed_uint << std::endl;

	std::cout << "seed=" << generator << " rnddata["<<n<<"]( ";
	for (int i=0; i<n; ++i)
		std::cout << distribution(generator) << " ";
	std::cout << ")" << std::endl;

	std::cout << "generator seed after " << n << " numbers: " << generator << std::endl;

	generator.seed( seed_uint );

	std::cout << "restore seed: " << generator << std::endl;

	std::cout << "seed=" << generator << " rnddata["<<n<<"]( ";
	for (int i=0; i<n; ++i)
		std::cout << distribution(generator) << " ";
	std::cout << ")" << std::endl;

	std::cout << "final seed: " << generator << std::endl;

	return 0;
}

//EOF
