Distributed generation of sequentially generated random numbers
===============================================================

This directory contains some examples I developed on how to distribute the seed for sequential random generation of data using c++11 generators.

.. code::

	gen.cpp:		basic serial version as PoC
	gen-mpi.cpp:		basic MPI version as PoC
	gen-mpi-omp.cpp:	complete MPI (+openMP) version

One issue requested on the exercises (if I understood correctly) was to distribute the random data generated on a single process (a) and/or distribute the generation of random data on different processes (b), but assuring that data generated using either (a) or (b) are identical. This implies starting from the same constant seed and find a way to continue the sequence of random data generation from the same point on a different independent process.

Using c++11, extracting the seed from a generator is possible, but by using standard srand()/rand() routines, there's no way I'm aware of to extract the current seed, so any process would need to generate all the random data from the same seed up to the point where it can save its own portion of data and then stop, or, what I used in the exercises (D1,D2,D3,D4), one process generates all the data and then distributes subportions of the data to their respective processes.

Both non-c++11 solutions imply waste of resources: computing power in the first case, memory in the second.
Distributing the seed, even though the generation is still sequential (any proc rank>0 need to wait for proc rank-1 to complete), there's no need to waste memory or computing/power resources.

+-------+-----------------------+----------------------------------+----------------------------+
|       | all processes         | one proc generates all and       | c++11 seed distribution    |
|       | generate N*(rank+1)   | distributes portions (scatterv)  | using send/recv            |
+-------+-----------------------+----------------------------------+----------------------------+
|time   | gen(N)                | gen(N) + scatterv((N/npes)*npes) | gen(N) + sendrecv(1*npes)  |
+-------+-----------------------+----------------------------------+----------------------------+
|energy | sum[0,np]{N*(rank+1)} | gen(N)                           | gen(N)                     |
+-------+-----------------------+----------------------------------+----------------------------+
|memory | N/npes                | N+(N/npes) (on generator)        | N/npes                     |
+-------+-----------------------+----------------------------------+----------------------------+

The makefile available in the current directory allows to compile the 3 executable (just type "make"), and test the 3 version ("make test").

#EOF
