/*
** baro - 20150328
*/

#include <iostream>
#include <random>
#include <sstream>
#include <unistd.h>
#include <mpi.h>

#ifdef _OPENMP
# include <omp.h>
#endif

typedef struct {
	int id;
	int nth;
	int max;
	int lim;
} omp_t;

typedef struct {
	int next;
	int prev;
	int curr;
} proc_t;

typedef struct {
	int rank;
	int size;
	int hostname_len;
	char hostname[MPI::MAX_PROCESSOR_NAME];
	proc_t proc;
} mpi_t;

typedef struct {
	int * base_sizes;
	int * base_offsets;
	int * ghost_sizes;
	int * ghost_offsets;
} scagat_s;

scagat_s sg = { NULL , NULL , NULL , NULL } ;

mpi_t mpi = {
	.rank = 0 ,
	.size = 1 ,
	.hostname_len = 0 ,
	.hostname = { '\0' } ,
	.proc = { .next = 0 , .prev = 0 , .curr = 0 }
};

omp_t omp = { .id = 0 , .nth = 1 , .max = 1 , .lim = 1 };


// this should ensures output is flushed and next line(s) will be printed alone
#define FLUSH do{MPI::COMM_WORLD.Barrier();usleep(10000);}while(0)


unsigned generate ( unsigned seed , int n , double * storage )
{
	std::default_random_engine generator( seed );
	std::uniform_real_distribution<double> distribution( 0.0 , 100.0 );
	for ( int i = 0 ; i < n ; i++ )
		storage[i] = distribution( generator );
	std::stringstream ss;
	ss << generator;
	ss >> seed;
	return seed;
}


void ring_distribution( unsigned seed , int n , double * storage )
{

#ifdef _OPENMP
# pragma omp single
{ /* start of "single" section, if accidentally invoked by a parallel region */
#endif

	if ( mpi.rank == 0 )
	{
		seed = generate( seed , n , storage );
		MPI::COMM_WORLD.Send( &seed , 1 , MPI::UNSIGNED , mpi.proc.next , mpi.proc.next );
		MPI::COMM_WORLD.Recv( &seed , 1 , MPI::UNSIGNED , mpi.proc.prev , mpi.proc.curr );
	}
	else
	{
		MPI::COMM_WORLD.Recv( &seed , 1 , MPI::UNSIGNED , mpi.proc.prev , mpi.proc.curr );
		seed = generate( seed , n , storage );
		MPI::COMM_WORLD.Send( &seed , 1 , MPI::UNSIGNED , mpi.proc.next , mpi.proc.next );
	}
	MPI::COMM_WORLD.Barrier();

#ifdef DEBUG
	std::cerr
		<< __FUNCTION__ << "():"
		<< " mpi.host=" << mpi.hostname
		<< " mpi.rank=" << mpi.rank
		<< " mpi.size=" << mpi.size
# ifdef _OPENMP
		<< " omp.id=" << omp.id
		<< " omp.nth=" << omp.nth
# endif
		<< " mpi.proc.prev=" << mpi.proc.prev
		<< " mpi.proc.curr=" << mpi.proc.curr
		<< " mpi.proc.next=" << mpi.proc.next
		<< " seed=" << seed
		<< std::endl;
#endif

	MPI::COMM_WORLD.Bcast( &seed , 1 , MPI::UNSIGNED , 0 );

#ifdef _OPENMP
} /* end of "single" section */
#endif

	return;
}


void print_vec_terse( double * a , int dim , const char * label )
{
	if ( label != NULL )
		std::cout << label;
	std::cout << "[" << dim << "](";
	for ( int i = 0 ; i < dim ; i++ )
	{
		std:: cout << a[i];
		if ( i != dim-1 )
			std::cout << ',';
	}
	std::cout << ")\n";
}


bool compare_vec( const double * a , const double * b , int dim , double tol )
{
	double diff = 0.0;
	for ( int i = 0 ; i < dim ; i++ )
	{
		diff = fabs(a[i]-b[i]);
		if ( diff > tol )
		{
			std::cerr
				<< "*** " << __FUNCTION__ << "(): vectors differ:"
				<< " a(" << i << ")=" << a[i]
				<< " b(" << i << ")=" << b[i]
				<< " diff=" << diff
				<< std::endl;
			return false;
		}
	}
	return true;
}


void balance( int npes , int n )
{
	int mod = n % npes;
	int loc = n / npes;

	sg.base_sizes    = new int[npes];
	sg.base_offsets  = new int[npes];
	sg.ghost_sizes   = new int[npes];
	sg.ghost_offsets = new int[npes];

	for ( int pe = 0 ; pe < npes ; pe++ )
	{
		sg.base_sizes[pe]   = loc;
		sg.base_offsets[pe] = sg.base_sizes[pe] * pe + mod;

		if ( pe < mod )
		{
			sg.base_sizes[pe]   += 1;
			sg.base_offsets[pe] += pe - mod;
		}

		sg.ghost_sizes[pe]   = sg.base_sizes[pe] + 2;
		sg.ghost_offsets[pe] = sg.base_offsets[pe];
	}
}


char * build_par_label( const char * label )
{
	size_t len = 128 + MPI::MAX_PROCESSOR_NAME + strlen(label);
	char * ret = (char *)malloc( len * sizeof(char) );
	memset( ret , 0x0 , sizeof(char) * len );
	snprintf( ret , len-1
			, "mpi.host=%s mpi.rank=%d mpi.size=%d "
#ifdef _OPENMP
			  "omp.id=%d omp.nth=%d "
#endif
			  "%s"
			, mpi.hostname , mpi.rank , mpi.size
#ifdef _OPENMP
			,  omp.id , omp.nth
#endif
			, label
		);
	return ret;
}


#if defined(DEBUG) && defined(_OPENMP)
void verify_no_parallel_mess( unsigned seed , int n , double * storage )
{
	#pragma omp parallel
	{
		omp.id  = omp_get_thread_num();
		omp.nth = omp_get_num_threads();
		std::cerr
			<< __FUNCTION__ << "():"
			<< " mpi.host=" << mpi.hostname
			<< " mpi.rank=" << mpi.rank
			<< " mpi.size=" << mpi.size
			<< " omp.id=" << omp.id
			<< " omp.nth=" << omp.nth
			<< " testing openMP parallel call to ring_distribution()"
			<< std::endl;
		ring_distribution( seed , n , storage );
	}
}
#endif


int main( int argc , char * argv[] )
{
	unsigned seed = 1U;

	size_t dim = 23;

	MPI::Init(argc, argv);
	mpi.rank = MPI::COMM_WORLD.Get_rank();
	mpi.size = MPI::COMM_WORLD.Get_size();

	memset( mpi.hostname , 0x0 , MPI::MAX_PROCESSOR_NAME );
	MPI::Get_processor_name( mpi.hostname , mpi.hostname_len );
	memset( mpi.hostname + mpi.hostname_len , 0x0 , MPI::MAX_PROCESSOR_NAME - mpi.hostname_len );

	mpi.proc.next = ( mpi.rank + 1 + mpi.size ) % mpi.size;
	mpi.proc.prev = ( mpi.rank - 1 + mpi.size ) % mpi.size;
	mpi.proc.curr = mpi.rank;

#ifdef _OPENMP
	omp.id  = omp_get_thread_num();		// doesn't really make sense outside parallel region
	omp.nth = omp_get_num_threads();	// doesn't really make sense outside parallel region
	omp.max = omp_get_max_threads();
	omp.lim = omp_get_thread_limit();
#endif

	switch (argc)
	{
		case 2:
			if ( argv[1][0] == '-' && argv[1][1] == 'h' )
			{
				if ( mpi.rank == 0 )
				{
					std::cerr << std::endl;
					std::cerr << "Usage: OMP_NUM_THREADS=X mpirun [...] " << argv[0] << " [SIZE|-h]" << std::endl;
					std::cerr << std::endl;
					std::cerr << "\tSIZE	2, 4, 8, 16, ..." << std::endl;
					std::cerr << "\t	default size: " << dim << std::endl;
					std::cerr << std::endl;
				}
				MPI::Finalize();
				exit(0);
			}
			dim = atol( argv[1] );
			break;
	}

	balance( mpi.size , dim );

	std::cerr
		<< "host=" << mpi.hostname << " rank=" << mpi.rank << " size=" << mpi.size
		<< " loc_n=" << sg.base_sizes[mpi.rank] << " offt=" << sg.base_offsets[mpi.rank]
		<< " ghost_loc_n=" << sg.ghost_sizes[mpi.rank] << " ghost_offt=" << sg.ghost_offsets[mpi.rank]
		<< std::endl;

	FLUSH;

	size_t loc_n = sg.base_sizes[mpi.rank];
	double * b_part = new double[loc_n];
	double * b_tot = NULL;
	double * b_ver = NULL;

	if ( mpi.rank == 0 )
	{
		b_tot = new double[dim];
		b_ver = new double[dim];
		generate( seed , dim , b_tot );
	}

	MPI::COMM_WORLD.Barrier();

#if defined(DEBUG) && defined(_OPENMP)
	verify_no_parallel_mess( seed , loc_n , b_part );
#else
	ring_distribution( seed , loc_n , b_part );
#endif

	MPI::COMM_WORLD.Barrier();

	MPI::COMM_WORLD.Gatherv( b_part , sg.base_sizes[mpi.rank] , MPI::DOUBLE , b_ver , sg.base_sizes , sg.base_offsets , MPI::DOUBLE , 0 );

	char * label = build_par_label("b_part");

	print_vec_terse( b_part , loc_n , label );

	FLUSH;

	if ( mpi.rank == 0 )
	{
		free(label);
		label = build_par_label("b_tot=");
		print_vec_terse( b_tot , dim , label );
		compare_vec( b_tot , b_ver , dim , 1e-20 );
	}

	free(label);

	MPI::Finalize();

	return 0;
}

//EOF
