/*
** baro - 20141107
*/

#ifndef _UTILS_H_
#define _UTILS_H_

#include "balance.h"

typedef struct {
	int rank;
	int size;
} mpi_t;

typedef struct {
	size_t n;	// loc_n
	size_t o;	// offset
	size_t bstart;	// start of block
	size_t bend;	// end of block
	scagat_s sg;	// scatterv/gatherv sizes and offsets arrays
} loc_t;

extern mpi_t mpi;
extern loc_t loc;

#endif /* _UTILS_H_ */

//EOF
