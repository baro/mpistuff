/*
** baro - 20141107
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <malloc.h>
#include <mpi.h>
#include <string.h>
#include <unistd.h>
#include "balance.h"
#include "utils.h"

mpi_t mpi = { .rank=0, .size=1 };

loc_t loc = {
	.n=0 ,
	.o=0 ,
	.bstart = 0,
	.bend = 0,
	.sg = { NULL , NULL }
};

#ifdef USE_OPT_PARSER
int opt( int argc , char * const * argv , size_t * sz )
{
	if ( argc != 2 || argv[1] == NULL || ! isdigit( *argv[1] ) )
	{
		fprintf( stderr , "Usage: %s SIZE\n" , argv[0] );
		return -1;
	}
	*sz = atoi( argv[1] );
	return 0;
}
#endif

int main( int argc , char * argv[] )
{
	int NP = 13;
	int i;
	double tstart, tend;
#ifdef USE_OPT_PARSER
	int error = 0;
#endif

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &mpi.size);
	MPI_Comm_rank(MPI_COMM_WORLD, &mpi.rank);

#ifdef USE_OPT_PARSER
	error = ( rank == 0 && opt( argc , argv , &NP ) == -1 );
	MPI_Bcast( &error , 1 , MPI_INT , 0 , MPI_COMM_WORLD );
	if ( error != 0 )
	{
		MPI_Finalize();
		exit(0);
	}
	MPI_Bcast( &NP , 1 , MPI_INT , 0 , MPI_COMM_WORLD );
#else
	if ( argc == 2 )
	{
		if ( mpi.rank == 0 )
			NP = atoi(argv[1]);
		MPI_Bcast( &NP , 1 , MPI_INT , 0 , MPI_COMM_WORLD );
	}
#endif

	if ( mpi.rank == 0 )
		fprintf( stderr , "%s: using %d MPI processes\n" , argv[0] , mpi.size );

	tstart = MPI_Wtime();

	balance ( mpi.size , mpi.rank , NP , &loc.bstart , &loc.bend , &loc.n );
	loc.sg = get_scagat_info( mpi.size , NP );

	loc.o = get_offset( mpi.size , mpi.rank , NP );
	fprintf( stderr , "p%d: loc.n=%zd loc.o=%zd bstart=%zd bend=%zd\n" , mpi.rank , loc.n , loc.o , loc.bstart , loc.bend );

	for (i = loc.bstart; i <= loc.bend; i++)
		fprintf( stderr , "p%d:  i=%d\n" , mpi.rank , i );

	tend = MPI_Wtime();

	sg_free( loc.sg );

	if ( mpi.rank == 0 )
		fprintf( stderr , "# total elapsed time: %f s\n", tend-tstart);

	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
	return 0;
}

//EOF
