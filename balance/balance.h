/*
** baro - 20141107
*/

#ifndef _BALANCE_H_
#define _BALANCE_H_

typedef struct {
	int * sizes;
	int * offsets;
} scagat_s;

void balance ( int npe , int rank , size_t n , size_t * start , size_t * end , size_t * loc_n );
size_t get_loc_n( int npe , int rank , size_t n );
size_t get_offset( int npe , int rank , size_t n );
scagat_s get_scagat_info( int npe , size_t size );
void sg_free( scagat_s sg );

#endif /* _BALANCE_H_ */

//EOF
