/*
** baro - 20141107
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "balance.h"
#include "utils.h"

/************************************************************
** compute balanced blocks, as described during the lecture
*/
/*
** balanced distribution of the rest
**
**	N=7	size=4	N%size=3 N/size=1
**	rank=0	loc_n=2	start=rank*loc_n stop=start+loc_n
**	rank=1	loc_n=2	start=rank*loc_n stop=start+loc_n
**	rank=2	loc_n=2	start=rank*loc_n stop=start+loc_n
**	rank=3	loc_n=1	start=rank*loc_n stop=start+loc_n
**
**	N=14	size=4	N%size=2 N/size=3
**	rank=0	loc_n=4
**	rank=1	loc_n=4
**	rank=2	loc_n=3
**	rank=3	loc_n=3
**
**	 0  1  2  3  4  5  6  7  8  9 10 11 12 13
**	p0 p0 p1 p1 p2 p2 p3
**	p0 p0 p0 p0 p1 p1 p1 p1 p2 p2 p2 p3 p3 p3
*/
// rank <  mod => loc_n = div+1
// rank >= mod => loc_n = div
// rank <  mod => start = (rank * (div+1))
// rank >= mod => start = (mod * (div+1)) + ((rank-mod) * div)
/*
** the code below is actually a contracted version of the
** following (considering as well start_block):
**	if ( n % npe == 0 )
**		loc_n = n / npe;
**	else
**		if ( th_id < ( n % npe ) )
**			loc_n = ( n / npe ) + 1;
**		else
**			loc_n = n / npe;
*/
void balance ( int npe , int rank , size_t n , size_t * start , size_t * end , size_t * loc_n )
{
	int mod = n % npe;
	int div = n / npe;
	*loc_n = div;
	*start = (rank * div) + mod;
	if ( rank < mod )
	{
		*loc_n += 1;
		*start += rank - mod;
	}
	*end = *start + *loc_n - 1;
	fprintf( stderr , "%s(): rank=%-2d pe=%-2d npe=%d n=%lu mod=%d div=%d loc_n=%lu sb=%lu eb=%lu l=%lu\n"
	                , __FUNCTION__ , rank , rank+1 , npe , n , mod , div , *loc_n
	                , *start , *end , (*end)-(*start)+1 );
}
size_t get_loc_n( int npe , int rank , size_t n )
{
	int mod = n % npe;
	int div = n / npe;
	return (size_t)(div + ( rank < mod ));
}
size_t get_offset( int npe , int rank , size_t n )
{
	int mod = n % npe;
	int div = n / npe;
	size_t start = (rank * div) + mod;
	if ( rank < mod )
		start += rank - mod;
	return (size_t)start;
}
// elements returned by this function must be free()
scagat_s get_scagat_info( int npe , size_t size )
{
	register int pe;
	scagat_s sg;
//	int o = 0;
	int s = 0;
	sg.sizes   = (int *)malloc( npe * sizeof(int) );
	sg.offsets = (int *)malloc( npe * sizeof(int) );
	sg.sizes[0]    = get_loc_n( npe , 0 , size );
	sg.offsets[0]  = 0;
#ifdef DEBUG
	fprintf( stderr , "%s(): tid=0 pe=1 size=%d offset=%d\n"
	                , __FUNCTION__ , sg.sizes[0] , sg.offsets[0] );
#endif
	for ( pe = 1 ; pe < npe ; pe++ )
	{
		s = get_loc_n( npe , pe , size );
//		o = get_offset( npe , pe , size );
		sg.sizes[pe]   = s ;
		sg.offsets[pe] = sg.offsets[pe-1] + sg.sizes[pe-1];
#ifdef DEBUG
		fprintf( stderr , "%s(): tid=%d pe=%d size=%d offset=%d\n"
		                , __FUNCTION__ , pe , pe+1
		                , sg.sizes[pe] , sg.offsets[pe] );
#endif
	}
	return sg;
}
void sg_free( scagat_s sg )
{
	if ( sg.offsets != NULL )
		free( sg.offsets );
	if ( sg.sizes != NULL )
		free( sg.sizes );
}

//	int npes, rank;
//	size_t start_block, end_block;
//	size_t loc_n;
//	scagat_s sg_info = { NULL , NULL };
//	...
//	balance ( npes , rank , NP , &start_block , &end_block , &loc_n );
//	sg_info = get_scagat_info( mpi.size , NP );
//	...
//	MPI_*v ( ... , loc_n , ... , sg_info.sizes , sg_info.offsets , ... );
//	...
//	sg_free( sg_info );

//EOF
